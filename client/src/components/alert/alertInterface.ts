export type Error = {
    msg: string
    param?: string
}
