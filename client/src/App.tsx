import React from 'react'
import './App.css'
import { Route, Routes } from 'react-router-dom'
import SignUp from './features/register/SignUp'
import SignIn from './features/login/SignIn'
import ProtectedRoute from './components/ProtectedRoute'
import Check from './features/Check'

function App() {
    return (
        <div className="App">
            <Routes>
                <Route path="/signup" element={<SignUp />} />
                <Route path="/signin" element={<SignIn />} />
                <Route element={<ProtectedRoute />}>
                    <Route index element={<Check />} />
                </Route>
                <Route path="*" element={<h1>404 NOT FOUND</h1>} />
            </Routes>
        </div>
    )
}

export default App
