import { Router } from 'express'
import { body } from 'express-validator'
import { getUserDetails, loginUser } from '../controllers/user.controller'
import { isAuthorised } from '../middlewares/auth.middleware'
import wrapAsync from '../utils/wrapAsnyc'

const loginRouter = Router()

/**
@route    POST api/auth
@desc     Authenticate user & get token
@access   Public
*/
loginRouter.post(
    '/',
    body('email', 'Please include a valid email').isEmail(),
    body('password', 'Password is required').exists(),
    wrapAsync(loginUser)
    /**
     * #swagger.summary = 'Login user'
     * #swagger.description = 'get auth token by providing correct email and password'
     * #swagger.tags = ['user']
        #swagger.parameters['requestBody'] = {
            in: 'body',
            description: 'login user.',
            schema: { $ref: '#/definitions/login' },
            required: true
        } 
     * #swagger.responses[201] = {
            description: 'login successfull, authorization token in response',
            schema: { $ref: '#/definitions/token' }
        }
     * #swagger.responses[400] = {
            description: 'validation error, ensure fields are correct',
            schema: { $ref: '#/definitions/validationError' }
        }
     * #swagger.responses[503] = {
            description: 'database error',
            schema: { $ref: '#/definitions/customError' }
        }
     * #swagger.responses[500] = {
            description: 'internal error',
            schema: { $ref: '#/definitions/customError' }
        }
    */
)

loginRouter.get(
    '/',
    isAuthorised(),
    wrapAsync(getUserDetails)
    /**
     * #swagger.summary = 'get user profile from token'
     * #swagger.description = 'get user email, name and role from token'
     * #swagger.tags = ['user']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
     * #swagger.responses[200] = {
            description: 'user details',
            schema: { $ref: '#/definitions/token' }
        }
     * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
     * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
     * #swagger.responses[503] = {
            description: 'database error',
            schema: { $ref: '#/definitions/customError' }
        }
     * #swagger.responses[500] = {
            description: 'internal error',
            schema: { $ref: '#/definitions/customError' }
        }
    */
)

export default loginRouter
