import { Types } from "mongoose";
export interface User {
  name: string;
  email: string;
  password: string;
  avatar: string;
  joiningDate: Date;
}

export interface ReqUser {
  _id: string;
}

export interface Chat {
  name: string;
  isGroupChat: boolean;
  users: User[];
  adminId: Types.ObjectId;
}

export interface Message {
  content: string;
  senderId: Types.ObjectId;
  chatId: Types.ObjectId;
  sent: Boolean;
  deliveredTo: Types.ObjectId[];
  readBy: Types.ObjectId[];
}
