import { Schema, model } from "mongoose";
import { Chat } from "../interfaces";
import { userSchema } from "./user.model";

export const chatSchema = new Schema<Chat>({
  name: {
    type: String,
    required: true,
  },
  isGroupChat: {
    type: Boolean,
    required: true,
  },
  users: [
    {
      type: userSchema,
    },
  ],
  adminId: {
    type: Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
});

export const ChatModel = model<Chat>("chat", chatSchema);
