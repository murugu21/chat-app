import { Schema, model } from "mongoose";
import { User } from "../interfaces";

export const userSchema = new Schema<User>({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
  },
  joiningDate: {
    type: Date,
    default: Date.now,
  },
});

export const UserModel = model<User>("User", userSchema);
